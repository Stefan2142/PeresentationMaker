﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using FFMpegCore;
using FFMpegCore.FFMPEG;
using MahApps.Metro.Controls;
using Microsoft.Win32;

namespace PresentationGenWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<List<string>> slides = new List<List<string>>();
        private List<string> txt_content = new List<string>();
        private OpenFileDialog OpenFileDialog1 = new OpenFileDialog();
        private List<string> available_movie_files = new List<string>();
        private string movie_directory;

        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        public MainWindow()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();


            mePlayer.LoadedBehavior = MediaState.Manual;
            mePlayer.UnloadedBehavior = MediaState.Stop;
            sliProgress.IsMoveToPointEnabled = true;

            movie_directory = @"C:\Users\stefa\source\repos\PresentationMaker\PresentationMaker\bin\Debug\Movies";

        }
        
        #region movie_player
        private void timer_Tick(object sender, EventArgs e)
        {
            if ((mePlayer.Source != null) && (mePlayer.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = mePlayer.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = mePlayer.Position.TotalSeconds;
            }
        }
        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Media files (*.mp4;*.mov)|*.mp4;*.mov;|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                mePlayer.Source = new Uri(openFileDialog.FileName);
        }

        private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (mePlayer != null) && (mePlayer.Source != null);
        }

        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Source = new Uri(movie_directory + "\\" + lbx_slide_movies.SelectedItem);
            mePlayer.Play();
            mediaPlayerIsPlaying = true;
        }

        private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Pause();
        }

        private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Stop();
            mediaPlayerIsPlaying = false;
        }

        private void SliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void SliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            mePlayer.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void SliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
        }

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            mePlayer.Volume += (e.Delta > 0) ? 0.1 : -0.1;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(lbx_slide_movies.SelectedItem != null)
            {
                mePlayer.Source = new Uri(movie_directory + "\\" + lbx_slide_movies.SelectedItem);
                mePlayer.Play();
                mediaPlayerIsPlaying = true;
            }
        }
        #endregion

        private void Btn_file_picker_Click(object sender, RoutedEventArgs e)
        {
            //Clearing everything before;
            slides.Clear();
            txt_content.Clear();
            lbx_slide_movies.Items.Clear();
            lstv_slides.Items.Clear();
            tbox_current_slide.Clear();
            mediaPlayerIsPlaying = false;
            mePlayer.Stop();
            sliProgress.Value = 0;


            OpenFileDialog1.Filter = "Txt files|*.txt;";
            OpenFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();

            if (OpenFileDialog1.ShowDialog().Equals(true))
            {
                try
                {
                    var sr = new StreamReader(OpenFileDialog1.FileName);
                    //text_area.Text = sr.ReadToEnd();
                    while (!sr.EndOfStream)
                        txt_content.Add(sr.ReadLine());
                }

                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}", "PresentationGen");
                }
            }
            

            text_area.Text = string.Join(Environment.NewLine, txt_content);

            available_movie_files = Directory.GetFiles(movie_directory).ToList();

            //Tries to find movie clips for each of the slides
            slides.Clear();
            for (int i = 0; i < txt_content.Count(); i++)
            {
                slides.Add(Movie_File_Helper(txt_content, available_movie_files, i));
                //slide_recordings.add();
            }

            //Add slide# & movie clips belonging to that slide (two columns)
            for (int i = 0; i < slides.Count; i++)
            {
                string[] items = new string[2];
                items[0] = "Slide " + i.ToString();
                items[1] = string.Join(", ", slides[i]);

                //ListViewItem lstItem = new ListViewItem(items);
                lstv_slides.Items.Add(new MyItem { slideNbr = items[0], videos = items[1] });
                //lstv_slides.Items.Add(lstItem);
            }
        }

        private List<string> Movie_File_Helper(List<string> txt_files, List<string> fl_list, int i)
        {
            var tmp = new List<string>();
            var sentence = new List<string>();
            var tmp_fl_list = new List<string>();

            foreach (string el in fl_list)
            {
                tmp_fl_list.Add(el.Split(new string[] { "\\" }, StringSplitOptions.None).Last());
            }
            sentence = split_string(txt_content[i]);
            foreach (string el in sentence)
            {
                for (int n = 0; n < tmp_fl_list.Count(); n++)
                {
                    if (tmp_fl_list[n].ToString().ToLower().Contains(el.ToLower()))
                    {
                        tmp.Add(tmp_fl_list[n]);
                    }
                }
            }
            //if (tmp.Count == 0) { tmp.Add("Empty"); }
            if (tmp.Count == 0) {}

            return tmp;
        }
        private List<string> split_string(string line)
        {
            var ret = new List<string>();
            string[] tmp = line.Trim().Split(new char[0]);
            foreach (string el in tmp)
            {
                if (el.Length > 4) { ret.Add(el); }
            }
            return ret;
        }

        private void lstv_slides_Click(object sender, EventArgs e)
        {
 
            var item = (sender as ListView).SelectedItem;
            if (item != null)
            {
                int selectedIndex = lstv_slides.SelectedIndex;
                try
                {
                    // Get text of the current slide
                    tbox_current_slide.Text = txt_content[selectedIndex];

                    // Populate list that holds movies for a current slide
                    lbx_slide_movies.Items.Clear();
                    TimeSpan time1 = new TimeSpan(0, 0, 0);
                    for (int i = 0; i < slides[selectedIndex].Count; i++)
                    {
                        time1 += new VideoInfo(movie_directory + "\\" + slides[selectedIndex][i]).Duration;
                        lbx_slide_movies.Items.Add(slides[selectedIndex][i]);
                    }
                    lbx_slide_movies.SelectedIndex = 0;
                    lbl_slide_len.Content = $"Slide duration: {time1.ToString()}";
                }
                catch(ArgumentOutOfRangeException ex)
                {
                    
                }
            }                 
        }

        private void Btn_add_to_current_slide_Click(object sender, RoutedEventArgs e)
        {
            int selectedIndex = lstv_slides.SelectedIndex;
            if (selectedIndex == -1)
            {
                MessageBox.Show("You must select a slide!", "PresentationGen");
                return;
            }
            OpenFileDialog1.Filter = "Media files (*.mp4;*.mov)|*.mp4;*.mov;|All files (*.*)|*.*";
            OpenFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();

            if (OpenFileDialog1.ShowDialog().Equals(true))
            {
                try
                {
                    var tmp = System.IO.Path.GetFileName(OpenFileDialog1.FileName);
                    slides[lstv_slides.SelectedIndex].Add(tmp);

                    //Add slide# & movie clips belonging to that slide (two columns)
                    lstv_slides.Items.Clear();
                    for (int i = 0; i < slides.Count; i++)
                    {
                        string[] items = new string[2];
                        items[0] = "Slide " + i.ToString();
                        items[1] = string.Join(", ", slides[i]);

                        //ListViewItem lstItem = new ListViewItem(items);
                        lstv_slides.Items.Add(new MyItem { slideNbr = items[0], videos = items[1] });
                        //lstv_slides.Items.Add(lstItem);
                    }


                    // populate list box that holds movies for current slide
                    TimeSpan time1 = new TimeSpan(0, 0, 0);
                    lbx_slide_movies.Items.Clear();                    
                    for (int i = 0; i < slides[selectedIndex].Count; i++)
                    {
                        lbx_slide_movies.Items.Add(slides[selectedIndex][i]);
                        time1 += new VideoInfo(movie_directory + "\\" + slides[selectedIndex][i]).Duration;
                    }
                    lbl_slide_len.Content = $"Slide duration: {time1.ToString()}";
                }

                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}", "PresentationGen");
                    lstv_slides.SelectedIndex = selectedIndex;
                }
                lstv_slides.SelectedIndex = selectedIndex;
            }

        }

        private void Btn_rem_to_current_slide_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = lstv_slides.SelectedIndex;
            if (selectedIndex == -1 || lbx_slide_movies.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a slide and a video to remove it!", "PresentationGen");
                return;
            }


            if (slides[selectedIndex].Count != 0)
            {
                slides[selectedIndex].RemoveAt(lbx_slide_movies.SelectedIndex);
                //Add slide# & movie clips belonging to that slide (two columns)
                lstv_slides.Items.Clear();
                for (int i = 0; i < slides.Count; i++)
                {
                    string[] items = new string[2];
                    items[0] = "Slide " + i.ToString();
                    items[1] = string.Join(", ", slides[i]);

                    //ListViewItem lstItem = new ListViewItem(items);
                    lstv_slides.Items.Add(new MyItem { slideNbr = items[0], videos = items[1] });
                    //lstv_slides.Items.Add(lstItem);
                }

                // populate list box that holds movies for current slide
                lbx_slide_movies.Items.Clear();
                TimeSpan time1 = new TimeSpan(0, 0, 0);
                for (int i = 0; i < slides[selectedIndex].Count; i++)
                {
                    time1 += new VideoInfo(movie_directory + "\\" + slides[selectedIndex][i]).Duration;
                    lbx_slide_movies.Items.Add(slides[selectedIndex][i]);
                }
                lbl_slide_len.Content = $"Slide duration: {time1.ToString()}";
                lstv_slides.SelectedIndex = selectedIndex;
            }
        }

        private void Btn_move_up_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = lstv_slides.SelectedIndex;
            if (selectedIndex == -1 || lbx_slide_movies.SelectedIndex == -1)
            {
                MessageBox.Show("You must select a slide and a video to move it up!", "PresentationGen");
                return;
            }

            if (lbx_slide_movies.SelectedIndex > 0)
            {
                var tmp = lbx_slide_movies.SelectedIndex;
                slides[selectedIndex].Insert(tmp - 1, lbx_slide_movies.SelectedItem.ToString());
                slides[selectedIndex].RemoveAt(lbx_slide_movies.SelectedIndex+1);
                // add a duplicate item up in the listbox
                lbx_slide_movies.Items.Insert(tmp - 1, lbx_slide_movies.SelectedItem.ToString());
                // delete the old occurrence of this item
                lbx_slide_movies.Items.RemoveAt(lbx_slide_movies.SelectedIndex);
                lbx_slide_movies.SelectedIndex = tmp-1;
            }

        }

        private void Btn_move_down_Click(object sender, RoutedEventArgs e)
        {
            var selectedIndex = lstv_slides.SelectedIndex;
            if (selectedIndex == -1 || lbx_slide_movies.SelectedIndex == -1)
            {
                //MessageBox.Show.Title("PresentationGen - ")
                
                    
                MessageBox.Show("You must select a slide and a video to move it down!", "PresentationGen");
                
                return;
            }
            
            if (lbx_slide_movies.SelectedIndex < lbx_slide_movies.Items.Count-1)
            {

                var tmp = lbx_slide_movies.SelectedIndex;
                slides[selectedIndex].Insert(tmp + 2, lbx_slide_movies.SelectedItem.ToString());
                slides[selectedIndex].RemoveAt(tmp);


                // add a duplicate item up in the listbox
                lbx_slide_movies.Items.Insert(tmp + 2, lbx_slide_movies.SelectedItem.ToString());
                // delete the old occurrence of this item
                lbx_slide_movies.Items.RemoveAt(tmp);
                lbx_slide_movies.SelectedIndex = tmp+1;


            }
        }
    }
}


//Used only for adding items to listview
public class MyItem
{
    public string slideNbr { get; set; }

    public string videos { get; set; }
}